
var pezcado = angular.module('pezcado', [
    'ui.router',                    // Routing
    'oc.lazyLoad',                  // ocLazyLoad
    'ui.bootstrap',                 // Ui Bootstrap
    'ngCookies',                    // ng cookies
    'oitozero.ngSweetAlert',      // sweetalert
	'angularjs-dropdown-multiselect',
	'ngTextTruncate'
	
]);


