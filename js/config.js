pezcado
    .config(config)
    .run(['$rootScope', '$state', 'factories', '$cookies', function ($rootScope, $state, factories, $cookies) {
        $rootScope.$state = $state;
        $rootScope.logout = function () {
            factories.logoutForm('Do you want to log out ?', function () {
                $state.go('login');
                $cookies.remove('token');
                factories.success('Logged out successfully');
            });
        };

        /*$rootScope.$on('$stateChangeStart', function (event, toState) {
                    console.log($state.current.name)
                if ( $state.current.name === 'order.pastOrder' ) 
                    {
                        console.log("Hey")
                       window.location.reload();
                    }
                    })*/
    }]);

function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, RouteHelpersProvider, APP_REQUIRES, $sceProvider) {
    $urlRouterProvider.otherwise("/login");
    $sceProvider.enabled(false);

    $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: APP_REQUIRES.modules
    });

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: "views/external/login.html",
            data: { pageTitle: 'Login' },
            controller: 'LoginCtrl',
            resolve: RouteHelpersProvider.resolveFor('login-controller')
        })
        .state('parent', {
            abstract: true,
            url: "/parent",
            templateUrl: "views/parent/common/content.html"
        })
        .state('parent.dashboard', {
            url: "/dashboard",
            templateUrl: "views/parent/dashboard.html",
            data: { pageTitle: 'dashboard' },
            controller: 'filtersCtrl',
            resolve: RouteHelpersProvider.resolveFor('filtersCtrl-controller', 'bootstrapLightbox', 'ngFileUpload')
        })
        .state('parent.viewsubcategory', {
            url: "/viewsubcategory",
            templateUrl: "views/parent/viewsubcategory.html",
            data: { pageTitle: 'View Sub category' },
            controller: 'viewsubcategoryCtrl',
            resolve: RouteHelpersProvider.resolveFor('viewsubcategoryCtrl-controller', 'bootstrapLightbox', 'ngFileUpload')
        })

        .state('parent.vendorList', {
            url: "/vendorList",
            templateUrl: "views/parent/vendorList.html",
            data: { pageTitle: 'Vendor List' },
            controller: 'vendorListCtrl',
            resolve: RouteHelpersProvider.resolveFor('vendorListCtrl-controller', 'ui.select', 'bootstrapLightbox', 'ngFileUpload')
        })

        .state('category', {
            abstract: true,
            url: "/category",
            templateUrl: "views/parent/common/content.html"
        })
        .state('category.list', {
            url: "/list",
            templateUrl: "views/parent/categoryList.html",
            data: { pageTitle: 'Category List' },
            controller: 'categoryListCtrl',
            resolve: RouteHelpersProvider.resolveFor('categoryListCtrl-controller', 'ngFileUpload', 'bootstrapLightbox')
        })
        .state('category.create', {
            url: "/create",
            templateUrl: "views/parent/createCategory.html",
            data: { pageTitle: 'Create Category' },
            controller: 'createCategoryCtrl',
            resolve: RouteHelpersProvider.resolveFor('createCategoryCtrl-controller', 'ngFileUpload', 'ngAutocomplete', 'ngTagsInput', 'daterangepicker')
        })
       
        .state('product', {
            abstract: true,
            url: "/product",
            templateUrl: "views/parent/common/content.html"
        })
        .state('product.list', {
            url: "/list",
            templateUrl: "views/parent/productList.html",
            data: { pageTitle: 'Product List' },
            controller: 'productListCtrl',
            resolve: RouteHelpersProvider.resolveFor('productListCtrl-controller', 'bootstrapLightbox', 'ngFileUpload')
        })
		 .state('parent.offer', {
            url: "/offer",
            templateUrl: "views/parent/offer.html",
            data: { pageTitle: 'Add Offer' },
            controller: 'offerCtrl',
            resolve: RouteHelpersProvider.resolveFor('offerCtrl-controller', 'bootstrapLightbox', 'ngFileUpload')
        })
        .state('order', {
            abstract: true,
            url: "/order",
            templateUrl: "views/parent/common/content.html"
        })
        .state('order.confirm', {
            url: "/confirmedList",
            templateUrl: "views/parent/confirm.html",
            data: { pageTitle: 'Confirmed Orders' },
            controller: 'confirmorderListCtrl',
            resolve: RouteHelpersProvider.resolveFor('confirmorderListCtrl-controller', 'bootstrapLightbox')
        })
        .state('order.cancel', {
            url: "/CanceledList",
            templateUrl: "views/parent/cancel.html",
            data: { pageTitle: 'Canceled Orders List' },
            controller: 'cancelOrderListCtrl',
            resolve: RouteHelpersProvider.resolveFor('cancelOrderListCtrl-controller', 'bootstrapLightbox')
        })
        .state('order.pending', {
            url: "/Pendinglist",
            templateUrl: "views/parent/pending.html",
            data: { pageTitle: 'Pending Orders List' },
            controller: 'pendingOrderListCtrl',
            resolve: RouteHelpersProvider.resolveFor('pendingOrderListCtrl-controller', 'bootstrapLightbox')
        })
     .state('order.deliver', {
            url: "/Deliverlist",
            templateUrl: "views/parent/deliver.html",
            data: { pageTitle: 'Deliver Orders List' },
            controller: 'deliverorderListCtrl',
            resolve: RouteHelpersProvider.resolveFor('deliverorderListCtrl-controller', 'bootstrapLightbox')
        })
        .state('user', {
            abstract: true,
            url: "/user",
            templateUrl: "views/parent/common/content.html"
        })
        .state('user.list', {
            url: "/list",
            templateUrl: "views/parent/userList.html",
            data: { pageTitle: 'User List' },
            controller: 'userListCtrl',
            resolve: RouteHelpersProvider.resolveFor('userListCtrl-controller', 'bootstrapLightbox')
        })
}

pezcado.config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider', '$injector',
    function ($controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider, $injector) {
        'use strict';

        // registering components after bootstrap
        pezcado.controller = $controllerProvider.register;
        pezcado.directive = $compileProvider.directive;
        pezcado.filter = $filterProvider.register;
        pezcado.factory = $provide.factory;
        pezcado.service = $provide.service;
        pezcado.constant = $provide.constant;
        pezcado.value = $provide.value;

    }]);
