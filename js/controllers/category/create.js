pezcado.controller('createCategoryCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout) {

    $scope.category = {
        name: '',
        desc: '',
    };
    $scope.screen = 'false';
    $scope.createCategory = function (isValid) {

        if (isValid) {

            $loading.start();
            services.createCategory($scope, function (response, status) {
                if (status == 1) {
                    $loading.finish();
                    factories.successCallback(response.message, function () {
                        $state.go('category.list');
                        //  $state.go($state.current, {}, { reload: true });
                    });
                }
                else {
                    $loading.finish();
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }
            });
        }
    };
    $scope.change = function () {
        console.log(new Date($scope.opTime).getTime());
    }
}]);