pezcado.controller('categoryListCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal) {

    /* Initialize list options */
    //$scope.search = '';
    $scope.listCategoryFunc = function () {

        $loading.start();
        services.listCategory(function (response, status) {
            if (status == 1) {

                $scope.categoryList = response.data;
                $loading.finish();

                factories.dataTable('listcat', [1, 4]);
                $scope.images = [];
                response.data.forEach(function (col) {
                    $scope.images.push({
                        url: col.categoryImage ? col.categoryImage.thumbnail : "img/no-image-available.jpg",
                        thumbUrl: col.categoryImage ? col.categoryImage.thumbnail : "img/no-image-available.jpg"
                    });
                });
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listCategoryFunc();

    $scope.addsubCategory = function (id) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'add-template.html',
            controller: 'addsubCategoryCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: id
                    };
                }
            }
        });
    };
    $scope.addproduct = function (id) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'add-product.html',
            controller: 'addproductCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: id
                    };
                }
            }
        });
    };
    $scope.updateCategory = function (details) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'update-template.html',
            controller: 'updateCategoryCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: details
                    };
                }
            }
        });
    };
    $scope.redirect = function (id) {
        window.location = "#/parent/viewsubcategory";
        $cookies.put('subcategory_id', id);
        //listSubCategoryFunc(id);
    }

    $scope.listSubCategoryFunc = function (details) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'challenge-template.html',
            controller: 'subCategoryCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: details
                    };
                }
            }
        });
    };
    $scope.blockUnblockCategory = function (id, sts, index) {
        if (sts === "block") {
            var message = "Do you want to block this Category ?";
        }
        else if (sts === "unblock") {
            var message = "Do you want to unblock this Category ?";
        }

        factories.confirm(message, function () {
            services.blockUnblockCategory(id, sts, function (response, status) {
                if (status == 1) {
                    factories.successCallback(response.message, function () {

                        if (sts == 'block') {
                            $scope.categoryList[index].isBlocked = true;
                        }
                        else {
                            $scope.categoryList[index].isBlocked = false;
                        }


                    });
                }

                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }
            });
        });
    };


    // $scope.removeSubCategory = function (id,$index) {
    //     factories.confirm("Do you want to remove this category ?",function () {
    //         services.removeSubCategory(id,function (response,status) {
    //             if(status == 1){
    //                 factories.successCallback(response.message,function () {
    //                     $scope.listCategoryFunc ();
    //                 });
    //             }
    //             else {
    //                 factories.error(response.message);
    //                 factories.unAuthorize(response);
    //             }
    //         });
    //     });
    // };
    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.images, index);
    };
}]);

pezcado.controller('addsubCategoryCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.details = {};
    $scope.details._id = items.details;

    // console.log(details)
    $scope.createsubCategory = function (isValid) {

        if (isValid) {
            $loading.start();
            services.createsubCategory($scope, function (response, status) {
                if (status == 1) {
                    $loading.finish();

                    factories.success("Added Successfully");
                    $scope.cancel();
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
pezcado.controller('updateCategoryCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.updatecategory = {};
    $scope.details = items.details;
    $scope.updatecategory.name = $scope.details.name;
    $scope.updatecategory.desc = $scope.details.description;
    $scope.updatecategory.otime = new Date($scope.details.openingTime);
    $scope.updatecategory.ctime = new Date($scope.details.closingTime);
    $scope.updatecategory.screen = $scope.details.differentScreen.toString();

    // $scope.file = $scope.details.categoryImage.thumbnail;
    // console.log(details)
    $scope.updateCategory = function (isValid) {

        if (isValid) {
            $loading.start();
            services.updateCategory($scope, function (response, status) {
                if (status == 1) {
                    $loading.finish();

                    factories.success("Updated Successfully");
                    $scope.cancel();
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
	$scope.flag='';
	 if($scope.details.openingTime>$scope.details.closingTime)
	$scope.flag=true;
	
	else 
	$scope.flag=false;
	
	$scope.time=function()

	{
	console.log('otime',$scope.updatecategory.otime)
	console.log('ctime',$scope.updatecategory.ctime)
	 if($scope.updatecategory.otime.getTime()>$scope.updatecategory.ctime.getTime())
	$scope.flag=true;
	
	else 
	$scope.flag=false;
	}
}]);
pezcado.controller('addproductCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.details = {};
    $scope.details._id = items.details;

    // console.log(details)
    $scope.createproduct = function (isValid) {

        if (isValid) {
            $loading.start();
            services.createproduct($scope, 'category', function (response, status) {
                if (status == 1) {
                    $loading.finish();

                    factories.success("Added Successfully");
                    $scope.cancel();
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);