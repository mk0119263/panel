pezcado.controller('filtersCtrl', ['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout','Lightbox','$uibModal',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout,Lightbox,$uibModal) {
	$scope.popup1 = {
            opened: false
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.open2 = function () {
            $scope.popup2.opened = true;
        };
		$scope.dateOptions = {
    	dateDisabled: disabled,
    	formatYear: 'yy',
    	minDate: $scope.start,
    	startingDay: 1
  	};

function disabled(data) {
      var date = data.date,
      mode = data.mode;
	  return date < $scope.start
  }

		$scope.listVendorFunc = function () {
        $loading.start();
        services.listVendor(function (response,status) {
        if(status == 1){
            $scope.vendorList = response.data;
			$loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listVendorFunc();
	
	$scope.changeData = function (){
    	
		if($scope.type != "Custom")
			{
				$loading.start();
			services.filters($scope,function (response,status) {
        	if(status == 1){
                $scope.filters = response.data;
				
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
			}
    };
	$scope.changeData();
	$scope.apply = function (){
	services.filters($scope,function (response,status) {
        	if(status == 1){
                $scope.filters = response.data;
				
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
	};
	$scope.reset = function(){
		$state.go($state.current, {}, { reload: true });
	}
}]);