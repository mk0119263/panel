pezcado.controller('offerCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', 'Upload', '$timeout', 'Lightbox', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, Upload, $timeout, Lightbox) {
   $scope.offer={};

    // console.log(details)
    $scope.addoffer = function (isValid) {

        if (isValid) {
            $loading.start();
            services.offer($scope, function (response, status) {
                if (status == 1) {
                    $loading.finish();
                    factories.success("Success");
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
		}
    };
   
}]);