pezcado.controller('pendingOrderListCtrl', ['$scope', '$rootScope', '$cookies', 'services','socket', 'factories', '$state', '$loading', 'Lightbox', '$uibModal', function ($scope, $rootScope, $cookies, services,socket, factories, $state, $loading, Lightbox, $uibModal) {
    $scope.listOptions = {
        pageNo: 1,
        limit: "10",
        total: null,
        maxSize: 5,
        skip: 0,
        search: ''
    };
    $scope.pg_options = {
        start: '',
        end: ''
    };
	// $scope.exportData = function (id) {
      //      var blob = new Blob([document.getElementById(id).innerHTML], {
       //         type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        //    });
         //   saveAs(blob, id + "orders.xls");
     //   };
    //to Excel
        $scope.exportData = function () {
            alasql('SELECT * INTO XLSX("Pendingorders.xlsx",{headers:true}) FROM ?', [$scope.ordersexcel]);
        };
	  
   $scope.audio= new Audio('img/notify.mp3');
	$scope.pendingList;
	 socket.on('order', function (data) {
        // console.log(data);
	  // $scope.pendingList.push(data)
	
        $scope.audio.play();
        $scope.listOrders();
    });
	 
 
   
   $rootScope.$on('done', function () {
        $scope.listOrders();
    });
    $scope.listOrders = function () {
        $scope.listOptions.skip = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit;
        $loading.start();
        services.listOrders('PENDING', $scope, function (response, status) {
            if (status == 1) {
                $scope.pendingList = response.data.data;
                $loading.finish();

                if (response.data.count == undefined) {
                    $scope.listOptions.total = 0;
                }
                else {        

					$scope.ordersexcel=[];
					$scope.count=1;
				    $scope.pendingList.forEach(function (col) {
                                $scope.ordersexcel.push({
                                    "S.No": $scope.count++,
                                    "Order Number": col.orderNumber != undefined ? col.orderNumber : '--',    
                                    "Name": col.name != undefined ? col.name : '--',
                                    "Payment Method": col.orderMethod != undefined ? col.orderMethod : '--',
                                    "Contact No": col.phoneNo != undefined ? col.phoneNo : '--',
                                    "Amount": col.netAmount != undefined ? col.netAmount : '--',
                                    "Address": (col.addressLine2 != undefined && col.pinCode != undefined ) ? (col.addressLine1+','+ col.addressLine2+','+col.pinCode): '--',
                                    "City": col.city != undefined ? col.city : '--',
									})
									})

                    $scope.listOptions.total = response.data.count;
                }
                if ($scope.listOptions.search == '') {
                    if ($scope.listOptions.pageNo == 1) {
                        $scope.pg_options.start = 1;
                        $scope.pg_options.end = $scope.listOptions.limit;
                    } else {
                        $scope.pg_options.start = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit + 1;
                        $scope.pg_options.end = $scope.listOptions.limit * $scope.listOptions.pageNo;
                    }
                    if ($scope.pg_options.end > $scope.listOptions.total) {
                        $scope.pg_options.end = $scope.listOptions.total;
                    }
                    if ($scope.listOptions.total == 0) {
                        $scope.resultPaginationCount = 'No Data Found';
                    }
                    else {
                        $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results';
                    }
                }
                /* pagination query search is not empty*/
                else {
                    if (angular.isUndefined(response.data.count) || response.data.count == 0) {
                        $scope.resultPaginationCount = 'No Results Found';
                    } else {
                        if ($scope.listOptions.pageNo == 1) {
                            $scope.pg_options.start = 1;
                            $scope.pg_options.end = $scope.listOptions.limit;
                        } else {
                            $scope.pg_options.start = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit + 1;
                            $scope.pg_options.end = $scope.listOptions.limit * $scope.listOptions.pageNo;
                        }
                        if ($scope.pg_options.end > $scope.listOptions.total) {
                            $scope.pg_options.end = $scope.listOptions.total;
                        }
                        // $scope.pg_options.start = 0;
                        // $scope.pg_options.end = $scope.usersList.count;
                        $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results for search query';
                    }
                }

			

            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listOrders();
    $scope.pageChanged = function () {
        // $scope.listOptions.search = '';
        $scope.listOrders();
    };
    /* Function called by search button on-enter and on-button click*/
    $scope.pageChangedBySearch = function () {
        $scope.listOrders();
    };
    $scope.details = function (data) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'orderdetails.html',
            controller: 'detailCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: data
                    };
                }
            }
        });
    };
    $scope.action = function (id, type) {
        var msg;
        if (type == 'CONFIRMED') {
            msg = 'Are you sure to Confirm this Order?';
        }
        else {
            msg = 'Are you sure to Cancel this Order?';

        }
        factories.confirm(msg, function () {
            $loading.start();
            services.orderstatus(id, type, function (response, status) {
                if (status == 1) {
                    factories.success(response.message);
                    $rootScope.$emit('done');
                    $loading.finish();
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        });


    }

}]);
pezcado.controller('detailCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.detail = items.details;
    factories.dataTable('detail', []);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);