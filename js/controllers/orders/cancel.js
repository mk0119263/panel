pezcado.controller('cancelOrderListCtrl', ['$scope', '$cookies', 'services', 'factories', '$state', '$loading', 'Lightbox', '$uibModal', function ($scope, $cookies, services, factories, $state, $loading, Lightbox, $uibModal) {
    $scope.listOptions = {
        pageNo: 1,
        limit: "10",
        total: null,
        maxSize: 5,
        skip: 0,
        search: ''
    };
    $scope.pg_options = {
        start: '',
        end: ''
    };
	  $scope.exportData = function () {
            alasql('SELECT * INTO XLSX("Canceledorders.xlsx",{headers:true}) FROM ?', [$scope.ordersexcel]);
        };
    // $rootScope.$on('Userslist', function () {
    //     $scope.listUsers();
    // });

    $scope.listOrders = function () {
        $scope.listOptions.skip = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit;
        $loading.start();
        services.listOrders('CANCEL', $scope, function (response, status) {
            if (status == 1) {
                $scope.cancelList = response.data.data;
                $loading.finish();

                if (response.data.count == undefined) {
                    $scope.listOptions.total = 0;
                }
                else {
				$scope.ordersexcel=[];
					$scope.count=1;
				    $scope.cancelList.forEach(function (col) {
                                $scope.ordersexcel.push({
                                    "S.No": $scope.count++,
                                    "Order Number": col.orderNumber != undefined ? col.orderNumber : '--',    
                                    "Name": col.name != undefined ? col.name : '--',
                                    "Payment Method": col.orderMethod != undefined ? col.orderMethod : '--',
                                    "Contact No": col.phoneNo != undefined ? col.phoneNo : '--',
                                    "Amount": col.netAmount != undefined ? col.netAmount : '--',
                                    "Address": (col.addressLine2 != undefined && col.pinCode != undefined ) ? (col.addressLine1+','+ col.addressLine2+','+col.pinCode): '--',
                                    "City": col.city != undefined ? col.city : '--',
									})
									})
                    $scope.listOptions.total = response.data.count;
                }
                if ($scope.listOptions.search == '') {
                    if ($scope.listOptions.pageNo == 1) {
                        $scope.pg_options.start = 1;
                        $scope.pg_options.end = $scope.listOptions.limit;
                    } else {
                        $scope.pg_options.start = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit + 1;
                        $scope.pg_options.end = $scope.listOptions.limit * $scope.listOptions.pageNo;
                    }
                    if ($scope.pg_options.end > $scope.listOptions.total) {
                        $scope.pg_options.end = $scope.listOptions.total;
                    }
                    if ($scope.listOptions.total == 0) {
                        $scope.resultPaginationCount = 'No Data Found';
                    }
                    else {
                        $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results';
                    }
                }
                /* pagination query search is not empty*/
                else {
                    if (angular.isUndefined(response.data.count) || response.data.count == 0) {
                        $scope.resultPaginationCount = 'No Results Found';
                    } else {
                        if ($scope.listOptions.pageNo == 1) {
                            $scope.pg_options.start = 1;
                            $scope.pg_options.end = $scope.listOptions.limit;
                        } else {
                            $scope.pg_options.start = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit + 1;
                            $scope.pg_options.end = $scope.listOptions.limit * $scope.listOptions.pageNo;
                        }
                        if ($scope.pg_options.end > $scope.listOptions.total) {
                            $scope.pg_options.end = $scope.listOptions.total;
                        }
                        // $scope.pg_options.start = 0;
                        // $scope.pg_options.end = $scope.usersList.count;
                        $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results for search query';
                    }
                }


            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listOrders();
    $scope.pageChanged = function () {
        // $scope.listOptions.search = '';
        $scope.listOrders();
    };
    /* Function called by search button on-enter and on-button click*/
    $scope.pageChangedBySearch = function () {
        $scope.listOrders();
    };
    $scope.details = function (data) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'orderdetails2.html',
            controller: 'detailCtrl2',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: data
                    };
                }
            }
        });
    };

}]);
pezcado.controller('detailCtrl2', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.detail2 = items.details;
    if ($.fn.DataTable.isDataTable('#detail')) {
        angular.element('#detail').DataTable().clear().destroy();
    }
    factories.dataTable('detail', []);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);