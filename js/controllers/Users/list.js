pezcado.controller('userListCtrl', ['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout','Lightbox','$uibModal',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout,Lightbox,$uibModal) {
 $scope.listOptions = {
            pageNo: 1,
            limit: 10,
            total: null,
            maxSize: 5,
            skip: 0,
            search: ''
        };
        $scope.pg_options = {
            start: '',
            end: ''
        };

      


        /* function to generate table with pagination */
        $scope.listusers = function () {
            if ($scope.listOptions.pageNo == 1) {
                $scope.listOptions.skip = 0 * $scope.listOptions.limit;
            } else {
                $scope.listOptions.skip = ($scope.listOptions.pageNo - 1) * $scope.listOptions.limit;
            }
            $loading.start();
            services.Userdata( $scope, function (response, status) {
                if (status == 1) {
                    if (response.data.userData) {
                        $scope.usersList = response.data.userData;
                    }
                    else {
                        $scope.usersList = [];
                    }
                    if (response.data.count == undefined) {
                        $scope.listOptions.total = 0;
                    }
                    else {
                        $scope.listOptions.total = response.data.count;
                    }
                    /* pagination query when search list is empty */
                    if ($scope.listOptions.search == '') {
                        if ($scope.listOptions.pageNo == 1) {
                            $scope.pg_options.start = 1;
                            $scope.pg_options.end = $scope.listOptions.limit;
                        } else {
                            $scope.pg_options.start = $scope.listOptions.skip + 1;
                            $scope.pg_options.end = $scope.listOptions.skip + $scope.listOptions.limit;
                        }
                        if ($scope.pg_options.end > $scope.listOptions.total) {
                            $scope.pg_options.end = $scope.listOptions.total;
                        }

                        if ($scope.listOptions.total == 0) {
                            $scope.resultPaginationCount = 'No Data Found';
                        }
                        else {
                            $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results';

                        }
                    }
                    /* pagination query search is not empty*/
                    else {
                        
						if ($scope.listOptions.pageNo == 1) {
                            $scope.pg_options.start = 1;
                            $scope.pg_options.end = $scope.listOptions.limit;
                        } else {
                            $scope.pg_options.start = $scope.listOptions.skip + 1;
                            $scope.pg_options.end = $scope.listOptions.skip + $scope.listOptions.limit;
                        }
                        if ($scope.pg_options.end > $scope.listOptions.total) {
                            $scope.pg_options.end = $scope.listOptions.total;
                        }

						
						if (angular.isUndefined($scope.usersList.length) || $scope.usersList.length == 0) {
                            $scope.resultPaginationCount = 'No Results Found';
                        } 
						
						else {
                           
                             $scope.resultPaginationCount = 'Showing ' + $scope.pg_options.start + ' to ' + $scope.pg_options.end + ' of Total ' + $scope.listOptions.total + ' Results for search query';
                        }
                    }
                    $loading.finish();
                } else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        };

        /* Call Function to paginate */
        $scope.listusers();

        /* function to call after page change */
        $scope.pageChanged = function () {
         
            $scope.listusers();
        };
        /* Function called by search button on-enter and on-button click*/
        $scope.pageChangedBySearch = function () {
            $scope.listusers();
        };
     $scope.blockUnblockuser = function (id, sts, index) {
        if (sts === "block") {
            var message = "Do you want to block this User?";
        }
        else if (sts === "unblock") {
            var message = "Do you want to unblock this User ?";
        }

        factories.confirm(message, function () {
            services.blockUnblockuser(id, sts, function (response, status) {
                if (status == 1) {
                    factories.successCallback(response.message, function () {
                        if (sts == 'block') {
                            $scope.usersList[index].isBlocked = true;
                        }
                        else {
                            $scope.usersList[index].isBlocked = false;

                        }

                    });
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }

            });
        });
    };

}]);
