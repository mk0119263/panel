pezcado.controller('LoginCtrl', ['$scope', '$cookies', 'services', 'factories', '$state', '$loading', function ($scope, $cookies, services, factories, $state, $loading) {

    $scope.user = {
        email: '',
        password: ''
    };


    $scope.login = function (isValid) {
        if (isValid) {
            $loading.start();
            services.login($scope, function (response, status) {
                if (status == 1) {
                    $cookies.put('token', response.data.access_token);
                    $loading.finish();
                    $state.go('order.pending');

                }
                else {
                    $loading.finish('login');
                    factories.error(response.message);
                    factories.unAuthorize(response);

                    $state.transitionTo('login', {}, { location: true, notify: false });
                }
            });
        }
    };
}]);