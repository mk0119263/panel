pezcado.controller('feedListCtrl', ['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout','Lightbox','$uibModal',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout,Lightbox,$uibModal) {
  
	$scope.listFeedFunc = function () {
        $loading.start();
        services.listFeed(function (response,status) {
        if(status == 1){
            $scope.feedList = response.data;
                        factories.dataTable('feedinfo');
            $scope.images = [];
             $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listFeedFunc();
        $scope.detail = function (data){
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'detail.html',
            controller: 'detailCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details:data
                    };
                }
            }
        });

    };
	 $scope.editFeed = function (data){
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'editFeed.html',
            controller: 'editFeedCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        detail:data
                    };
                }
            }
        });

    };
	
}]);
pezcado.controller('editFeedCtrl',['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout','Lightbox','$uibModal','$uibModalInstance','items',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout,Lightbox,$uibModal,$uibModalInstance,items) {
	$scope.data = items.detail;
	$scope.feed = {
      dish: $scope.data.multiDetails[0].dishName,
	  owner: $scope.data.multiDetails[0].createdBy,
	  dish1: $scope.data.multiDetails[1].dishName,
	  owner1: $scope.data.multiDetails[1].createdBy 
    };
	/*var data = items.detail.details; 
	$scope.challengeArr = [];
	$scope.challenges.forEach(function (data) {
                var tempObj =  {
                    "languageId": "58982b8ee6807c04409beaf3",
                    "title": data.title,
					"description": data.description
                     };
          		var tempObj1 = {
                    "languageId": "58770dcfa2fff286144fff39",
                    "title": data.title,
					"description": data.description
                     };
                challengeArr.push(tempObj);
				challengeArr.push(tempObj1);
				
            });
	*/
	$scope.challenges= [];
	console.log($scope.data.details);
	for(var i = 1; i<= $scope.data.details.length; )
		{
			console.log("Hello");
			var tempObj = {
			};
			
			for(var j = 1; j<=2; j++)
				{
//					var n= "title"+"j";
//					var nn= "description"+"j";
					console.log("enter in loop"+i +j);
					tempObj.title = $scope.data.details[j-1].title;
					tempObj.description = $scope.data.details[j-1].description;
					
                    console.log(j);
                  	
			var merged_object = angular.extend({}, $scope.challenges, tempObj);
					console.log(merged_object);
					
					}
			
			
			
			
			
			
//					$scope.challenges.push(tempObj);
					console.log($scope.challenges);	
				
			i=i+2;
			
		}
	
	/*angular.forEach($scope.data.details, function(value, key){
		
		var tempObj =  {
                    "languageId": "58982b8ee6807c04409beaf3",
                    "title": value.title,
					"description": value.description
                     };
		var tempObj1 =  {
                    "languageId": "58770dcfa2fff286144fff39",
                    "title": value.title,
					"description": value.description
                     };
          		/*var tempObj1 = {
                    
                    "title": value.title,
					"description": value.description
                     };*/
				
						

				/*console.log(tempObj1);
					
                        $scope.challenges.push(tempObj);
						$scope.challenges.push(tempObj1);
                        
                    });*/
	/*$scope.challenges = [{
		
		name: $scope.data.multiDetails[0].dishName,
		name1: $scope.data.multiDetails[0].dishName,
		desc: $scope.data.multiDetails[0].dishName,
		desc1: 	$scope.data.multiDetails[0].dishName,
		
	}];*/
	$scope.addChallenge = function () {
		
        $scope.challenges.push({});
    };
        $scope.data = items.details;
            $scope.cancel= function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
//detail 
pezcado.controller('detailCtrl',['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout','Lightbox','$uibModal','$uibModalInstance','items',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout,Lightbox,$uibModal,$uibModalInstance,items) {
	
        $scope.data = items.details;
            $scope.cancel= function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
