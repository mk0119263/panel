pezcado.controller('createFeedCtrl',
['$scope','$rootScope','$cookies','services','factories','$state','$stateParams','$loading','$timeout',function ($scope,$rootScope,$cookies,services,factories,$state,$stateParams,$loading,$timeout) {

	$rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams){
            if(fromState.controller == "pastOrderListCtrl")
            {
                window.location.reload();
                //$state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
            }
            else {
                localStorage.clear();
            }
    });
	$scope.feed = {
      dish:'',
	  owner:'',
	  dish1:'',
	  owner1:'' 
    };
	$scope.challenges = [{
		
	}];
	$scope.addChallenge = function () {
		console.log("enter in controller");
        $scope.challenges.push({});
    };
	console.log($scope.challenges);
    $scope.createFeed = function (isValid) {
		console.log("enter in controller");
		if(isValid){
			console.log("enter in loop")
			 
            $loading.start();
            services.createFeed($scope,function (response,status) {
                if(status == 1){
                   $loading.finish();
                   factories.successCallback(response.message,function () {
                       $state.go('feed.list');
                   });
               }
               else {
                   $loading.finish();
                   factories.error(response.message);
                   factories.unAuthorize(response);
               }
            });
		}
    };
}]);
