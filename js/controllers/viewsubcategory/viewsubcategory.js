pezcado.controller('viewsubcategoryCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal) {
    $scope.Category = '';
    $scope.listCategoryFunc = function () {
        $loading.start();
        services.listCategory(function (response, status) {
            if (status == 1) {
                $scope.categoryList = response.data;
                factories.dataTable('productlist');

                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listCategoryFunc();
    $scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.images, index);
    };

    $scope.listSubCategory = function (id) {
        $loading.start();
        services.listSubCategory(id, function (response, status) {
            if (status == 1) {
            
               $scope.subcategoryList = response.data;
                $scope.images = [];
                if (response.data.length) {
                    response.data.forEach(function (col) {
                        $scope.images.push({
                            url: col.subCategoryImage ? col.subCategoryImage.thumbnail : "img/no-image-available.jpg",
                            thumbUrl: col.subCategoryImage ? col.subCategoryImage.thumbnail : "img/no-image-available.jpg"
                        });
                    });
                }
                if ($.fn.DataTable.isDataTable('#listsubcat')) {
                    angular.element('#listsubcat').DataTable().clear().destroy();
                }
                factories.dataTable('listsubcat');
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };

    $scope.listSubCategory();


    $scope.changeData = function (id) {
        $scope.listSubCategory(id);
    };
    $scope.blockUnblockSubCategory = function (id, sts, index) {
        if (sts === "block") {
            var message = "Do you want to block this Subcategory ?";
        }
        else if (sts === "unblock") {
            var message = "Do you want to unblock this Subcategory ?";
        }

        factories.confirm(message, function () {
            services.blockUnblockSubCategory(id, sts, function (response, status) {
                if (status == 1) {
                    factories.successCallback(response.message, function () {
                        if (sts == 'block') {
                            $scope.subcategoryList[index].isBlocked = true;
                        }
                        else {
                            $scope.subcategoryList[index].isBlocked = false;

                        }

                    });
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }

            });
        });
    };

    $scope.delSubCategoryFunc = function (id) {
        factories.confirm("Do you want to remove this category ?", function () {
            services.removeSubSubCategory(id, function (response, status) {
                if (status == 1) {
                    factories.successCallback(response.message, function () {
                        $scope.listSubCategory();
                    });
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }
            });
        });
    };
    $scope.updateSubCategoryFunc = function (details) {
	
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'updatesubcategory.html',
            controller: 'updatesubCategoryCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: details
                    };
                }
            }
        });

    };

    $scope.addProductFunc = function (sid, cid) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'add-subproduct.html',
            controller: 'addsubProductCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        subcat: sid,
                        cat: cid
                    };
                }
            }
        });

    };
}]);
pezcado.controller('addsubProductCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', 'Upload', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, Upload, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
    $scope.details = {};
    $scope.details._id = items.cat;
    $scope.details.sid = items.subcat;

    // console.log(details)
    $scope.createproduct = function (isValid) {

        if (isValid) {
            $loading.start();
            services.createproduct($scope, 'subcategory', function (response, status) {
                if (status == 1) {
                    $loading.finish();

                    factories.success("Added Successfully");
                    $scope.cancel();
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);
pezcado.controller('updatesubCategoryCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', 'Upload', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, Upload, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
   $scope.category={};
   $scope.id=items.details._id
   $scope.category.name=items.details.name;
   $scope.category.desc=items.details.description;
	
    // console.log(details)
    $scope.updatesubcategory = function (isValid) {

        if (isValid) {
            $loading.start();
            services.updatesubcategory($scope, function (response, status) {
                if (status == 1) {
                    $loading.finish();
                    factories.success("Updated Successfully");
                    $scope.cancel();
                    $state.go($state.current, {}, { reload: true });

                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
		}
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);