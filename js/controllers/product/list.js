pezcado.controller('productListCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal) {
   $scope.Category='';
    $scope.listCategoryFunc = function () {
    //    $loading.start();
        services.listCategory(function (response, status) {
            if (status == 1) {

                $scope.categoryList = response.data;
              
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listCategoryFunc();
	$scope.openLightboxModal = function (index) {
        Lightbox.openModal($scope.images, index);
    };
    $scope.listProductFunc = function (sts) {
        $loading.start();
        services.listProduct($scope,sts, function (response, status) {
            if (status == 1) {
			
                $scope.ProductList = response.data;
				 $scope.images = [];
                if (response.data.length) {
                    response.data.forEach(function (col) {
                        $scope.images.push({
                            url: col.imageUrl ? col.imageUrl.original : "img/no-image-available.jpg",
                            thumbUrl: col.imageUrl ? col.imageUrl.thumbnail : "img/no-image-available.jpg"
                        });
                    });
                }
                if ($.fn.DataTable.isDataTable('#productlist')) {
                    angular.element('#productlist').DataTable().clear().destroy();
                }
                factories.dataTable('productlist');
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
    };
    $scope.listProductFunc();

  
    $scope.changeData = function (id) {
	$scope.subCategory=''
	$scope.cid=id;
        //        $cookies.put('subcategory_id',details);
		 $scope.listProductFunc('category');
     if($scope.Category.length!=0)
	{ $loading.start();
        services.listSubCategory(id, function (response, status) {
            if (status == 1) {
                $scope.subcategoryList = response.data;
                $loading.finish();
            }
            else {
                factories.error(response.message);
                factories.unAuthorize(response);
                $loading.finish();
            }
        });
		}
    };
    $scope.change = function (id) {
	    $scope.sid=id;
        $scope.listProductFunc('subcategory');
    };
	 $scope.blockUnblockproduct = function (id, sts, index) {
        if (sts === "block") {
            var message = "Do you want to block this Product ?";
        }
        else if (sts === "unblock") {
            var message = "Do you want to unblock this Product ?";
        }

        factories.confirm(message, function () {
            services.blockUnblockproduct(id, sts, function (response, status) {
                if (status == 1) {
                    factories.successCallback(response.message, function () {
                        if (sts == 'block') {
                            $scope.ProductList[index].isBlocked = 'true';
                        }
                        else {
                            $scope.ProductList[index].isBlocked = 'false';

                        }

                    });
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                }

            });
        });
    };

    $scope.updateProduct = function (details) {
        $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'update-template.html',
            controller: 'updateProductCtrl',
            size: '',
            resolve: {
                items: function () {
                    return {
                        details: details
                    };
                }
            }
        });

    };

}]);
pezcado.controller('updateProductCtrl', ['$scope', '$rootScope', '$cookies', 'services', 'factories', '$state', '$stateParams', '$loading', '$timeout', 'Lightbox', '$uibModal', '$uibModalInstance', 'items', function ($scope, $rootScope, $cookies, services, factories, $state, $stateParams, $loading, $timeout, Lightbox, $uibModal, $uibModalInstance, items) {
   $scope.update={};
   $scope.update.id=items.details._id;
   $scope.update.cid=items.details.categoryId;
   $scope.update.sid=items.details.subcategoryId;
   $scope.update.name=items.details.productName;
   $scope.update.description=items.details.description;
   $scope.update.minValue=items.details.minValue;
   $scope.update.basePriceUnit=items.details.basePriceUnit;
   $scope.file=items.details.imageUrl.original;
  
	
    $scope.updateProduct = function (isValid) {
        
        if (isValid) {
            services.updateProduct($scope, function (response, status) {

                if (status == 1) {
                    $loading.finish();
                    factories.successCallback(response.message, function () {
                        $scope.cancel();
                        $state.go($state.current, {}, { reload: true });
                    });
                }
                else {
                    factories.error(response.message);
                    factories.unAuthorize(response);
                    $loading.finish();
                }
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);