pezcado.service('services', ['$http', 'Api', '$timeout', '$cookies', function ($http, Api, $timeout, $cookies) {
    return {
        login: function ($scope, callback) {
            $http
                ({
                    url: Api.url + '/admin/login',
                    method: "POST",
                    data: {
                        "email": $scope.user.email,
                        "password": $scope.user.password
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        createCategory: function ($scope, callback) {

            var formData = new FormData();
            formData.append('name', $scope.category.name);
            formData.append('description', $scope.category.desc);
            formData.append('openingTime', new Date($scope.opTime).getTime());
            formData.append('closingTime', new Date($scope.clTime).getTime());
            formData.append('differentScreen', $scope.screen);
            formData.append('imageUrl', $scope.file);

            $http
                ({
                    url: Api.url + '/admin/addCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        createsubCategory: function ($scope, callback) {

            var formData = new FormData();
            formData.append('categoryId', $scope.details._id);
            formData.append('name', $scope.category.name);
            formData.append('description', $scope.category.desc);
            formData.append('imageUrl', $scope.file);

            $http
                ({
                    url: Api.url + '/admin/addSubCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });

        },
        createproduct: function ($scope, type, callback) {

            var formData = new FormData();
            if (type == 'category') {
                formData.append('categoryId', $scope.details._id);
                formData.append('productName', $scope.product.name);
                formData.append('basePriceUnit', $scope.product.price);
                formData.append('description', $scope.product.desc);
                formData.append('minValue', $scope.product.qty);
                formData.append('imageUrl', $scope.product.file);
            }
            if (type == 'subcategory') {
                formData.append('categoryId', $scope.details._id);
                formData.append('subcategoryId', $scope.details.sid);
                formData.append('productName', $scope.product.name);
                formData.append('basePriceUnit', $scope.product.price);
                formData.append('description', $scope.product.desc);
                formData.append('minValue', $scope.product.qty);
                formData.append('imageUrl', $scope.product.file);
            }

            $http
                ({
                    url: Api.url + '/admin/addProduct',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
				},
         updateProduct: function ($scope, callback) {
            var formData = new FormData();
			    formData.append('productId', $scope.update.id);
				if(!! $scope.update.cid)
                formData.append('categoryId', $scope.update.cid);
				if(!! $scope.update.sid)
                formData.append('subcategoryId', $scope.update.sid);
                formData.append('productName', $scope.update.name);
                formData.append('basePriceUnit', $scope.update.basePriceUnit);
                formData.append('description', $scope.update.description);
                formData.append('minValue', $scope.update.minValue);
                formData.append('imageUrl', $scope.file);
            $http
                ({
                    url: Api.url + '/admin/updateProduct',
                    method: "PUT",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        updateCategory: function ($scope, callback) {
            var formData = new FormData();
            formData.append('categoryId', $scope.details._id);
            formData.append('name', $scope.updatecategory.name);
            formData.append('description', $scope.updatecategory.desc);
            formData.append('openingTime', new Date($scope.updatecategory.otime).getTime());
            formData.append('closingTime', new Date($scope.updatecategory.ctime).getTime());
            formData.append('differentScreen', $scope.updatecategory.screen);
            formData.append('imageUrl', $scope.file);
            $http
                ({
                    url: Api.url + '/admin/updateCategory',
                    method: "PUT",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
		offer: function ($scope, callback) {
            var formData = new FormData();
            formData.append('name', $scope.offer.name);
            formData.append('description', $scope.offer.desc);
			  if(!!$scope.file)
			  formData.append('imageUrl', $scope.file);
            $http
                ({
                    url: Api.url + '/admin/addOffer',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
		updatesubcategory: function ($scope, callback) {
            var formData = new FormData();
            formData.append('subCategoryId', $scope.id);
            formData.append('name', $scope.category.name);
            formData.append('description', $scope.category.desc);
          if(!!$scope.file)
		  formData.append('image', $scope.file);
            $http
                ({
                    url: Api.url + '/admin/updateSubCategory',
                    method: "PUT",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: formData
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        listCategory: function (callback) {
            $http
                ({
                    url: Api.url + '/admin/getCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },

                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        listSubCategory: function (id, callback) {
            var data = {};
            if (id) {
                var data = { "categoryId": id };
            }
            $http
                ({
                    url: Api.url + '/admin/getSubCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: data
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        listProduct: function ($scope, sts, callback) {
            var data ;

            if (sts == 'category' && $scope.cid.length != 0) {
                 data = { "categoryId": $scope.cid };
            }
            else if (sts == 'subcategory') {
                 data = { "categoryId": $scope.cid, "subcategoryId": $scope.sid };
            }

            else
                data = {}
            $http
                ({
                    url: Api.url + '/admin/getProducts',
                    method: "GET",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    params: data
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        Userdata: function ($scope, callback) {
            var data = {};
            if ($scope.listOptions.search.length != 0) {
                data = {
                    "skip": $scope.listOptions.skip,
                    "limit": $scope.listOptions.limit,
                    "search": $scope.listOptions.search.toString()
                }
            }
            else {
                data = {
                    "skip": $scope.listOptions.skip,
                    "limit": $scope.listOptions.limit,

                }
            }
            $http
                ({
                    url: Api.url + '/admin/getAllUser',
                    method: "GET",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    params: data
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        listOrder: function (callback) {

            $http
                ({
                    url: Api.url + '/api/admin/viewUpcomingOrders',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        rejectOrder: function (callback) {

            $http
                ({
                    url: Api.url + '/api/admin/rejectedOrder',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        pastOrder: function (callback) {

            $http
                ({
                    url: Api.url + '/api/admin/pastOrders',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        currentOrder: function (callback) {

            $http
                ({
                    url: Api.url + '/api/admin/otherOrders',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        detailOrder: function (item, callback) {

            $http
                ({
                    url: Api.url + '/api/admin/orderDetails',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: {
                        "orderId": item.details
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },


        blockUnblockproduct: function (id, status, callback) {
            $http
                ({
                    url: Api.url + '/admin/blockUnblockProduct',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: {
                        "productId": id,
                        "blockUnblock": status
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        removeProduct: function (id, callback) {
            $http
                ({
                    url: Api.url + '/api/admin/deleteProducts',
                    method: "DELETE",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': 'application/json'
                    },
                    data: {
                        "productId": id,
                        "isDeleted": "true"

                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },






        blockUnblockCategory: function (id, status, callback) {
            $http
                ({
                    url: Api.url + '/admin/blockUnblockCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: {
                        "categoryId": id,
                        "blockUnblock": status
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        blockUnblockSubCategory: function (id, status, callback) {
            $http
                ({
                    url: Api.url + '/admin/blockUnblockSubCategory',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: {
                        "subCategoryId": id,
                        "blockUnblock": status
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        blockUnblockuser: function (id, status, callback) {
            $http
                ({
                    url: Api.url + '/admin/blockUnblockUser',
                    method: "PUT",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token')
                    },
                    data: {
                        "userId": id,
                        "action": status
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        removeSubCategory: function (id, callback) {
            $http
                ({
                    url: Api.url + '/api/admin/deleteSubCategories',
                    method: "DELETE",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': 'application/json'
                    },
                    data: {
                        "subCategoryId": id,
                        "isDeleted": "true"

                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

        removeSubSubCategory: function (id, callback) {
            $http
                ({
                    url: Api.url + '/api/admin/deleteDetailsSubCategory',
                    method: "DELETE",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': 'application/json'
                    },
                    data: {
                        "detailsSubCategoryId": id,
                        "isDeleted": "true"

                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        listOrders: function (type, $scope, callback) {
            var data;
            if ($scope.listOptions.search == '') {
                data = {
                    'limit': $scope.listOptions.limit.toString(),
                    'skip': $scope.listOptions.skip,
                    'action': type
                }
            } else {
                data = {
                    'limit': $scope.listOptions.limit.toString(),
                    'skip': $scope.listOptions.skip,
                    'search': $scope.listOptions.search.toString(),
                    'action': type
                }
            }
            $http
                ({
                    url:  Api.url + '/admin/getOrders',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                        'Content-Type': undefined
                    },
                    data: data
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },
        orderstatus: function (id, type, callback) {
            $http
                ({
                    url:  Api.url + '/admin/confirmOrder',
                    method: "POST",
                    headers: {
                        "authorization": 'bearer ' + $cookies.get('token'),
                    },
                    data: {
                        "orderId": id,
                        "action": type
                    }
                }).success(function (response) {
                    callback(response, 1);
                }).error(function (response) {
                    callback(response, 0);
                });
        },

    }
}]);

pezcado.factory('factories', ['SweetAlert', '$state', '$timeout', '$cookies', function (SweetAlert, $state, $timeout, $cookies) {
    return {
        success: function (message) {
            SweetAlert.swal({
                title: message,
                type: "success",
                timer: 2000
            });
        },
        error: function (message) {
            SweetAlert.swal({
                title: message,
                type: "error"
            });
        },
        successCallback: function (message, callback) {
            SweetAlert.swal({
                title: message,
                type: "success",
            }, function (isConfirm) {
                if (isConfirm) {
                    callback();
                }
            });
        },
        errorCallback: function (message, callback) {
            SweetAlert.swal({
                title: message,
                type: "danger"
            }, function () {
                callback();
            });
        },
        confirm: function (message, callback) {
            SweetAlert.swal({
                title: message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        callback();
                    }
                });
        },
        logoutForm: function (message, callback) {
            SweetAlert.swal({
                title: message,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        callback();
                    }
                });
        },
        unAuthorize: function (data) {
            if (data.statusCode == 401) {
                $state.go('login');
                $cookies.remove('obj');
            }
        },
        hideSideMenu: function () {
            angular.element('.pace-done').addClass('mini-navbar');
            $cookies.put('sidemenu', true);
        },
        dataTable: function (id, array) {
            $timeout(function () {
                // $('#' + id).DataTable();
                $('#' + id).DataTable({
                    "columnDefs": [
                        { "orderable": false, "targets": array }
                    ],
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
                });
            });
        },

    }
}]);
pezcado.service('$loading', ['$timeout', function ($timeout) {
    return {
        start: function () {
            $timeout(function () {
                pleaseWait({
                    backgroundColor: '#fed14e',
                    loadingHtml: "<div class='sk-spinner sk-spinner-chasing-dots'><div class='sk-dot1'></div><div class='sk-dot2'></div></div>"
                });
                angular.element('.pg-loading-logo-header').remove();
                angular.element('.pg-loading-screen .pg-loading-html').css({
                    "margin-top": "0px"
                });
                angular.element('.sk-spinner-chasing-dots.sk-spinner').css({
                    "width": "5-0px",
                    "height": "50px"
                });
            });
        },
        finish: function (timeout) {
            if (!timeout) {
                timeout = 0;
            }
            $timeout(function () {
                angular.element('body').removeClass('.pg-loading');
                angular.element('.pg-loading-screen').remove();
                angular.element('body.pg-loading').css({
                    "overflow": "visible"
                });
            }, timeout);
        }
    }
}]);
pezcado.factory('socket', function ($rootScope) {
    var socket = io.connect("54.191.54.252:8000");
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    }
});