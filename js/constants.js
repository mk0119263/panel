/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 =========================================================*/
pezcado.constant("Api", {
    "url": "http://54.191.54.252:8000"
});

pezcado.constant('APP_REQUIRES', {
    // jQuery based and standalone scripts
    scripts: {
        'login-controller': ['js/controllers/login/login.js'],
        'viewsubcategoryCtrl-controller': ['js/controllers/viewsubcategory/viewsubcategory.js'],
        'createCategoryCtrl-controller': ['js/controllers/category/create.js'],
        'categoryListCtrl-controller': ['js/controllers/category/list.js'],
        'userListCtrl-controller': ['js/controllers/Users/list.js'],
        'productListCtrl-controller': ['js/controllers/product/list.js'],    
        'filtersCtrl-controller': ['js/controllers/dashboard/filters.js'],
        'confirmorderListCtrl-controller': ['js/controllers/orders/confirm.js'],
        'deliverorderListCtrl-controller': ['js/controllers/orders/delivered.js'],
        'cancelOrderListCtrl-controller': ['js/controllers/orders/cancel.js'],
        'pendingOrderListCtrl-controller': ['js/controllers/orders/pending.js'],
		  'offerCtrl-controller': ['js/controllers/orders/offer.js'],
    },
    // Angular based script
    modules: [
        {
            name: 'ngFileUpload',
            files: ['js/plugins/ng-file-upload/ng-file-upload.min.js']
        },
        {
            name: 'bootstrapLightbox',
            files: ['js/plugins/angular-bootstrap-lightbox/angular-bootstrap-lightbox.min.css',
                'js/plugins/angular-bootstrap-lightbox/angular-bootstrap-lightbox.min.js']
        },
        {
            name: 'ui.select',
            files: ['js/plugins/angular-ui-select/dist/select.css',
                'js/plugins/angular-ui-select/dist/select.js']
        },
        {
            name: 'ngAutocomplete',
            files: ['js/plugins/ngAutocomplete/ngAutocomplete.js']
        },
        {
            name: 'ngTagsInput',
            files: ['js/plugins/ng-tags-input/ng-tags-input.css', 'js/plugins/ng-tags-input/ng-tags-input.js']
        },
        {
            name: 'daterangepicker',
            files: ['js/plugins/angular-daterange-picker/daterangepicker.js',
                'js/plugins/angular-daterange-picker/angular-daterangepicker.js', 'js/plugins/angular-daterange-picker/daterangepicker.css']
        }

    ]
});
